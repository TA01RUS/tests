Вывести всех людей, живущих в городе N

ORM
Person.objects.filter(city=city_id)

RAW

query = Person.objects.raw(""" 
select a.id, a.city_id, a.name, b.name as city_name from public."AppModels_person" a
join public."AppModels_city" b on a.city_id = b.id 
""")

Вывести всех людей, живущих в городе N

ORM
person_count = Person.objects.values_list('city_id').annotate(cnt=Count('id') ).order_by('-cnt', 'city_id')[:5] 

RAW
query = Person.objects.raw(""" 
select a.id, a.city_id, a.name from public."AppModels_person" a 
where a.city_id = %s
""", [city_id])


Вывести 5 городов с наибольшим населением, упорядочив по убыванию.

ORM

person_count = Person.objects.values_list('city_id').annotate(cnt=Count('id') ).order_by('-cnt', 'city_id')[:5] 
person_dict = { p[0]: p[1] for p in person_count }
object_list = City.objects.filter(id__in=[city[0] for city in person_count])

RAW

city = City.objects.raw( """ 
select * from ( select a.id, a.name, count(b.id) as count_person from public."AppModels_city" a
join public."AppModels_person" b on b.city_id = a.id
group by a.id, a.name ) a
order by count_person desc, id asc
limit 5
""") 

Реализовать пагинацию с сортировкой по разнице дат

ORM
days = Event.objects.all().values_list('id').annotate(days=F('end_date')-F('start_date')).order_by('-days') 

RAW
query = Event.objects.raw(""" 
select * from (
select a.id , a.start_date, a.end_date, DATE_PART('day', a.end_date-a.start_date) as days 
from public."AppModels_event" a 
) a
order by days desc, id asc
""") 