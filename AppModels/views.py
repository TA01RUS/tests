from django.core import paginator
from django.db.models.expressions import F
from AppModels.models import City, Event, Person
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView
from django.core.paginator import Paginator
from django.db.models import  Count

# Create your views here.


class  Home(TemplateView):
    template_name = 'home.html'  
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)        
        return  context    

# ORM 
class AllPersonORMViewList(ListView):
    """
        Вывести список людей и городов где они живут?
    """
    template_name = 'orm_all_person.html'
    paginate_by = 24
    model = Person


    
class PersonInCityORMViewList(ListView):
    """ 
        Вывести всех людей, живущих в городе N
    """    
    model = Person
    paginate_by = 24
    template_name = 'orm_person_in_city.html'

    def get_queryset(self) :
        city_id = self.kwargs.get('city_id')       

        query = Person.objects.filter(city=city_id)

        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)                
        city_id = self.kwargs.get('city_id')
        city    = City.objects.get(pk=city_id)                
        context['city'] = city
        return context

class TopCityTemplateView(TemplateView):
    """
        Вывести 5 городов с наибольшим населением, упорядочив по убыванию.
    """
    template_name = 'orm_top_city.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)                

        person_count = Person.objects.values_list('city_id').annotate(cnt=Count('id') ).order_by('-cnt', 'city_id')[:5]        

        person_dict = { p[0]: p[1] for p in person_count }
        object_list = City.objects.filter(id__in=[city[0] for city in person_count])        
        context['object_list'] = object_list
        context['person_dict'] = person_dict

        return context


class EventPaginationTemplateView(TemplateView):
    """ 
        Реализовать пагинацию с сортировкой по разнице дат
    """
    template_name   = 'orm_event_pagination.html'
    model           = Event
    paginate_by     = 24
    
    def get_context_data(self, **kwargs):
        page = self.request.GET.get('page', 1)        
        context = super().get_context_data(**kwargs)                

        days = Event.objects.all().values_list('id').annotate(days=F('end_date')-F('start_date')).order_by('-days')       

        days_dict = { day[0]: day[1].days for day in days }
        paginator   = Paginator(days, self.paginate_by)
        page_obj    = paginator.get_page(page)        
        events = Event.objects.filter(id__in=[id[0] for id in page_obj.object_list])
        context['object_list']  = events
        context['page_obj']     = page_obj        
        context['days_dict']    = days_dict        
        return context


class AllPersonRawiewList(ListView):
    """
        Вывести список людей и городов где они живут?
    """
    template_name = 'raw_all_person.html'
    paginate_by = 24    
    model = Person

    def get_queryset(self) :
        query = Person.objects.raw(""" 
                select a.id, a.city_id, a.name, b.name as city_name  from public."AppModels_person" a
                join   public."AppModels_city" b on a.city_id = b.id 
                """)
        return query
    
class PersonInCityRAWiewList(ListView):
    """ 
        Вывести всех людей, живущих в городе N
    """    
    model = Person
    paginate_by = 24
    template_name = 'raw_person_in_city.html'

    def get_queryset(self) :
        city_id = self.kwargs.get('city_id')               
        query = Person.objects.raw(""" 
                select a.id, a.city_id, a.name from public."AppModels_person" a                
                where a.city_id = %s
                """, [city_id])
        return query

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)                
        city_id = self.kwargs.get('city_id')
        city    = City.objects.raw( """ 
                    select a.id,  a.name, count(b.id) as count_person  from public."AppModels_city" a
                    join public."AppModels_person" b on b.city_id = a.id 
                    where a.id = %s
                    group by a.id, a.name
                """, [city_id])
        context['city'] = city[0]
        return context

class TopCityRawTemplateView(TemplateView):
    """
        Вывести 5 городов с наибольшим населением, упорядочив по убыванию.
    """
    template_name = 'raw_top_city.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)                

        city    = City.objects.raw( """ 
                    select * from (  select a.id,  a.name, count(b.id) as count_person  from public."AppModels_city" a
                    join public."AppModels_person" b on b.city_id = a.id
                    group by a.id, a.name ) a
                    order by count_person desc, id  asc
                    limit 5
                """)        
        context['object_list'] = city
        return context        

class EventPaginationRawTemplateView(ListView):
    """ 
        Реализовать пагинацию с сортировкой по разнице дат
    """
    template_name   = 'raw_event_pagination.html'
    model           = Event
    paginate_by     = 24

    def get_queryset(self) :
        query = Event.objects.raw(""" 
                select * from (
                        select a.id , a.start_date, a.end_date, DATE_PART('day',  a.end_date-a.start_date) as days  
                        from public."AppModels_event" a 
                    ) a
                    order by days desc, id asc
                """)        
        return query
    
    def get_context_data(self, **kwargs):        
        context = super().get_context_data(**kwargs)                        
        return context        
    
