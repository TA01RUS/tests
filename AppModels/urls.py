from django.urls import path, include
from .views import  EventPaginationTemplateView, Home, AllPersonORMViewList, PersonInCityORMViewList, \
                    TopCityTemplateView, AllPersonRawiewList, PersonInCityRAWiewList, TopCityRawTemplateView, EventPaginationRawTemplateView



app_name = 'test'
urlpatterns_orm = [
    path('person-all', AllPersonORMViewList.as_view(), name='orm-person-all'),
    path('city-persons/<int:city_id>', PersonInCityORMViewList.as_view(), name='orm-city-persons'),
    path('top-city', TopCityTemplateView.as_view(), name='orm-top-city'),
    path('paginator-event', EventPaginationTemplateView.as_view(), name='orm-paginator-event'),
]

urlpatterns_raw = [
    path('person-all', AllPersonRawiewList.as_view(), name='raw-person-all'),
    path('city-persons/<int:city_id>', PersonInCityRAWiewList.as_view(), name='raw-city-persons'),
    path('top-city', TopCityRawTemplateView.as_view(), name='raw-top-city'),
    path('paginator-event', EventPaginationRawTemplateView.as_view(), name='raw-paginator-event'),
]



urlpatterns = [
    path('', Home.as_view(), name='home'),
    path('orm/', include(urlpatterns_orm)),    
    path('raw/', include(urlpatterns_raw)),    
]