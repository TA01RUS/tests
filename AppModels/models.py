from django.db import models

# Create your models here.

class City(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name}'    
    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"
    
class Person(models.Model):
    name = models.CharField(max_length=100)    
    city = models.ForeignKey(City, verbose_name="Город", on_delete=models.PROTECT)
    class Meta:
        verbose_name = "Сотрудник"
        verbose_name_plural = "Сотрудники"


class Event(models.Model):
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    class Meta:
        verbose_name = "Событие"
        verbose_name_plural = "Спбытия"


