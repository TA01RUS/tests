from django import template
register = template.Library()
@register.simple_tag
def get_result(id: int , dict_list: dict):
    return dict_list[id]
