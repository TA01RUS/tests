"""    
    1 Вход параметры количество объектов (адресов, сотрудников, сообщений  )        
"""


import os
import sys
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'PyTests.settings')
import django
django.setup()
from AppModels.models import  City, Person, Event
import pytz

from faker import Faker

fakegen = Faker('ru_RU')

def generate(N=100):         
    City.objects.all().delete()  
    Person.objects.all().delete()     
    Event.objects.all().delete()     


    for entry in range(N):
        fake_address = fakegen.city()
        city = City.objects.get_or_create(name=fake_address)[0]                
        peples = fakegen.pyint(1, 5)
        for i in range(peples):
            fake_person_name  = fakegen.name()
            person = Person.objects.get_or_create(city=city, name=fake_person_name)[0]

        for e in range(N):
            from_date = fakegen.date_time(tzinfo=pytz.UTC)                        
            to_date = fakegen.date_time_between(start_date=from_date, tzinfo=pytz.UTC)            
            e = Event()            
            e.start_date = from_date
            e.end_date = to_date
            e.save()           


if __name__ == "__main__":    
    args = sys.argv[1:]
    if len(args) == 2 and args[0] == '-city':
        n = args[1]
        generate(int(n))
    else:
        generate()